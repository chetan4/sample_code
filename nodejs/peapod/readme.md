Viewing the project:
Open public/peapod/index.html in browser.

Optional installation steps for setting up a simple Node.js app that serves the static pages, and install dependencies for editing Sass files:

1. Install Node.js from http://nodejs.org/download/ if it's not installed.
2. npm install
3. cd public/peapod
4. npm install
5. Install grunt if not already installed using: npm install -g grunt-cli
6. grunt watch
7. cd to project root and run node app.js

Steps 3, 4, 5 and 6 are required only if you want to edit the Sass files in public/peapod/sass

Relevant files are in public/peapod.
