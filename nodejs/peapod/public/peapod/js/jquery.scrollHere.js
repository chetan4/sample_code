/*
  Author: Aniket A. Suryavanshi | @44Sur
  Gist URL: https://gist.github.com/anikets/b8dd21a374aebc1f1f37

  A handy jQuery plugin to scroll an element to top of screen.

  Example usage:
    $( '#selector' ).scrollHere();
    $( '#selector' ).scrollHere( 45 );
    $( '#selector' ).scrollHere( $( '.header' ).height());
    $( '#selector' ).scrollHere( 45, 1000, 'swing', function() {console.log('...'); });
*/

$.fn.scrollHere = function( topOffset, scrollSpeed, scrollEasing, callback ) {
  var offset = topOffset || 0,  // Optional param.
    speed = scrollSpeed || 500,
    easing = scrollEasing || 'swing',
    wholeBody = $("html, body");
  wholeBody.animate({ scrollTop: this.offset().top - offset }, speed, easing, callback );
};
