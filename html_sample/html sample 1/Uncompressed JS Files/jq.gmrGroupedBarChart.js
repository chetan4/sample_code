(function ( $, d3 ) {
    var GmrGroupedBarChart = function(el, config){
    var self = this;
    self.$element = $(el);
        
        var elWidth = Math.floor(self.$element.width()),
            elHeight = 300;

        self.config = config;
        self.data = config.data;

        elHeight = self.config.svg_height || 300;

        self.show_legend = (typeof self.config.show_legend === "undefined" ? true : self.config.show_legend );

        self.margin = {top: 10, right: 20, bottom: 30, left: 40};
        self.width = elWidth - self.margin.left - self.margin.right;

        self.height = elHeight - self.margin.top - self.margin.bottom;

        //self.$element.height(elHeight);

        if(self.config.colors){
            self.colors = self.config.colors;
        }else {
            self.colors = ["#ff6a75", "#ffbb6e", "#8b73f5", "#74c476", "#fdae6b", "#756bb1", "#31a354", "#636363", "#e6550d", "#3182bd", "#1f77b4", "#aec7e8", "#aec7e8"];
        }

self.svg = d3.select(el).append("svg")
            .attr("width", self.width + self.margin.left + self.margin.right)
            .attr("height", self.height + self.margin.top + self.margin.bottom)
            .append("g")
            .attr("transform", "translate(" + self.margin.left + "," + self.margin.top + ")");

            self.initTip();
            self.processKeys();
            self.createAxis();
            self.createChart();
            self.$element.addClass('gmr-grouped-bar-chart');
    };
    GmrGroupedBarChart.prototype.initTip = function() {
        var self = this;
        this.tip = d3.tip()
            .attr('class', 'd3-tooltip')
            .offset([-10, 0])
            .html(function(d) {
                return (self.config.tip_text || "") + d.value;
            });
        this.svg.call(this.tip);
    };
    GmrGroupedBarChart.prototype.processKeys = function() {
        var self = this;

        self.keys = d3.keys(self.data[0]).filter(function(key) { return key !== "label"; });

        self.data.forEach(function(d) {
            d.keys = self.keys.map(function(name) { return {name: name, value: +d[name]}; });
        });

        self.maxYAxis =  d3.max(self.data, function(d) { return d3.max(d.keys, function(d) { return d.value; }); });
    };
    GmrGroupedBarChart.prototype.createChart = function() {
        var self = this;

        self.x0.domain(self.data.map(function(d) { return d.label; }));
        self.x1.domain(self.keys).rangeRoundBands([0, self.x0.rangeBand()]);
        self.y.domain([0, self.maxYAxis]);

        self.svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + self.height + ")")
            .call(self.xAxis);

        self.svg.append("g")
            .attr("class", "y axis")
            .call(self.yAxis);

        var label = self.svg.selectAll(".label")
            .data(self.data)
            .enter().append("g")
            .attr("class", "g bar")
            .attr("transform", function(d) { return "translate(" + self.x0(d.label) + ",0)"; });

        label.selectAll("rect")
            .data(function(d) { return d.keys; })
            .enter().append("rect")
            .on('mouseover', self.tip.show)
            .on('mouseout', self.tip.hide)
            .attr("width", self.x1.rangeBand())
            .attr("x", function(d) { return self.x1(d.name); })
            .attr("y", function(d) { return self.y(d.value); })
            .attr("height", function(d) { return self.height - self.y(d.value); })
            .style("fill", function(d) { return self.color(d.name); });

        self.createLegends();

    };
    GmrGroupedBarChart.prototype.createLegends = function() {
        var self = this,
            domain;
        if(self.show_legend) {
            domain = self.color.domain();
            var str = "<div class='legendWrap'>";
            for(var i=0; i<domain.length; i++){
                str += "<span class='legendItem'><span style='background-color:"+self.colors[i]+";' class='legendSqure'></span>";
                str += domain[i];
                str += "</span>";
            }
            str += "</div>";
            this.$element.append(str);
        }
    };
    GmrGroupedBarChart.prototype.createAxis = function() {
        var self = this;
        self.x0 = d3.scale.ordinal().rangeRoundBands([0, self.width], 0.1);
        self.y = d3.scale.linear().range([self.height, 0]);

        self.x1 = d3.scale.ordinal();


        self.color = d3.scale.ordinal()
            .range(self.colors);


        self.xAxis = d3.svg.axis()
            .scale(self.x0)
            .orient("bottom");

        var ticks = self.config.ticks || 10;

        if(self.maxYAxis <= ticks){
            ticks = self.maxYAxis;
        }

        self.yAxis = d3.svg.axis()
            .scale(self.y)
            .orient("left")
            .ticks(ticks);
    };
 
    $.fn.gmrGroupedBarChart = function(config) {
    this.each(function(idx, item){
    $(item).data('gmr_grouped_bar_chart', new GmrGroupedBarChart(item, config));
    });
        return this;
    };
 
}( jQuery, window.d3 ));