(function ( $, d3 ) {
 		var GmrBarChart = function(el, config){
 				var self = this;
 				self.$element = $(el);
        
        var elWidth = Math.floor(self.$element.width()),
            elHeight = 300;

        self.config = config;
        self.data = config.data;
        if(self.config.show_legend) {
            self.margin = {top: 50, right: 20, bottom: 30, left: 40};
        }else {
            self.margin = {top: 10, right: 20, bottom: 30, left: 40};
        }

        var rotate_xaxis_text = self.config.rotate_xaxis_text || false;
        self.rotate_xaxis_text = rotate_xaxis_text;

        if(self.rotate_xaxis_text) {
            self.margin = {top: 10, right: 20, bottom: 100, left: 40};
        }else {
            self.margin = {top: 10, right: 20, bottom: 30, left: 40};
        }        

        self.width = elWidth - self.margin.left - self.margin.right;
        self.height = elHeight - self.margin.top - self.margin.bottom;

		self.svg = d3.select(el).append("svg")
            .attr("width", self.width + self.margin.left + self.margin.right)
            .attr("height", self.height + self.margin.top + self.margin.bottom);

            self.initTip();
            self.createAxis();
            self.createChart();
            if(self.config.show_legend) {
                self.createLegend();
            }
            self.$element.addClass('gmr-bar-chart');
 		};
    GmrBarChart.prototype.initTip = function() {
        var self = this;
        this.tip = d3.tip()
          .attr('class', 'd3-tooltip')
          .offset([-10, 0])
          .html(function(d) {
            return (self.config.tip_text || "") + d.value;
          });
        this.svg.call(this.tip);
    };
    GmrBarChart.prototype.createLegend = function() {
        var self = this,
            str = "<div class='legendWrap'>";
            str += "<span class='legendItem'><span style='background-color:"+(self.config.fill || '#8B73F5')+";' class='legendSqure'></span>";
            str += self.config.legend_text;
            str += "</span>";
            str += "</div>";
            this.$element.append(str);
    };
    GmrBarChart.prototype.createChart = function() {
        var self = this,
            gview;

        gview = self.svg.append("g").attr("transform", "translate(" + self.margin.left + "," + self.margin.top + ")");

        var xaxisView = gview.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + self.height + ")")
            .call(self.xAxis);


        if (self.rotate_xaxis_text){
            xaxisView.selectAll("text")
                .style("text-anchor", "end")
                .attr("dx", "-.82em")
                .attr("dy", "-5px")
                .attr("transform", "rotate(-90)");
        }

        var viw = gview.selectAll(".bar")
            .data(self.data)
            .enter().append("rect")
            .attr("class", "bar")
            .on('mouseover', self.tip.show)
            .on('mouseout', self.tip.hide)
            .attr("x", function(d) { return self.x(d.label); })
            .attr("width", (self.x.rangeBand()))
            .attr("y", function(d) { return self.y(d.value); })
            .attr("height", function(d) { return self.height - self.y(d.value); });

        if(self.config.fill){
            viw.style("fill", self.config.fill);
        }

        gview.append("g")
              .attr("class", "y axis")
              .call(self.yAxis);

    };
    GmrBarChart.prototype.createAxis = function() {
        var self = this;
        self.x = d3.scale.ordinal().rangeRoundBands([0, self.width], 0.1);
        self.y = d3.scale.linear().range([self.height, 0]);

        self.xAxis = d3.svg.axis()
            .scale(self.x)
            .orient("bottom");


        self.yAxis = d3.svg.axis()
            .scale(self.y)
            .orient("left")
            .ticks(10);

        self.x.domain(self.data.map(function(d) { return d.label; }));
        self.y.domain([0, d3.max(self.data, function(d) { return d.value; })]);
    };
 		
    $.fn.gmrBarChart = function(config) {
    		this.each(function(idx, item){
    				$(item).data('gmr_bar_chart', new GmrBarChart(item, config));
    		});
        return this;
    };
 
}( jQuery, window.d3 ));