(function ( $, d3 ) {
 		var GmrDonutChart = function(el, config){
            var self = this,
                ratio = 1.75;
            self.$element = $(el);
            self.config = config;
            self.data = config.data;
            self.elData = self.$element.data();
            self.width = Math.floor(self.$element.width());

            if(self.config.height){
                self.height = self.config.height;
            }else if(self.elData.use_height){
                self.height = Math.floor(self.$element.height());
            }else {
                self.height = Math.floor(self.width / ratio);
            }

            self.show_legend = (typeof self.config.show_legend === "undefined" ? true : self.config.show_legend );
            self.show_label = (typeof self.config.show_label === "undefined" ? true : self.config.show_label );
            self.show_polyline = (typeof self.config.show_polyline === "undefined" ? true : self.config.show_polyline );
            self.legend_percent = (self.config.legend_percent || false);
            self.hide_label = self.config.hide_label || false;
            self.apply_text_collapse = self.config.apply_text_collapse || false;
            self.radius = self.config.radius || (Math.min(self.width, self.height) / 2);
            self.is_money = (typeof self.config.is_money === "undefined" ? true : self.config.is_money );

            if(self.config.colors){
                self.colors = self.config.colors;
            }else {
                self.colors = ["#8b73f5", "#ff6a75", "#ffbb6e", "#74c476", "#756bb1", "#31a354", "#636363", "#e6550d", "#3182bd",
                    "#1f77b4", "#aec7e8", "#aec7e8", "#3499e0", "#949392", "#8a74b9", "#4cb992", "#efb57c",
                    "#6a4ca6", "#fdae6b", "#439FE0", "#3C76BC", "#f9b644"];
            }

            var total = 0;
            this.data.forEach(function(item){
               total += +(item.value);
            });

            this.data.forEach(function(item){
                var percent = ((item.value/total)*100);
                    percent = percent.toFixed(2);
                item.percent = percent + "%";
                item.lengend_label = item.label;
                if(self.elData.append_percent){
                        var t = self.is_money ? Number(item.value).formatMoney() : item.value;
                        var appendText = self.hide_label ? "" : (" - " + item.lengend_label);
                        item.label = (t + " (" + item.percent + ")" + (self.show_legend ? "" : appendText));
                }
            });

            if(!total){
                self.$element.html("NA");
                return false;
            }
            self.svg = self.createSVG(el);
            self.createPie();
            self.createArc();
            self.createOrdinal();
            self.createChart();
            var new_height = self.height + (self.$element.find(".legendWrap").outerHeight() || 0);
            self.$element.find("svg").height(self.height);
            self.$element.height(new_height);
            self.$element.addClass('gmr-donut-chart');
 		};
 		GmrDonutChart.prototype.createPie = function() {
                var self = this;
 				this.pie = d3.layout.pie().sort(self.config.sortFn).value(function(d) {
 				    return d.value;
 				});
 		};
 		GmrDonutChart.prototype.createOrdinal = function() {
            var self = this,
                colors = self.colors,
                domains = self.data.map(function(dt){
                    return dt.lengend_label;
                });

 				self.color = d3.scale.ordinal()
 				    .domain(domains)
 				    .range(colors);
 		};
 		GmrDonutChart.prototype.createArc = function() {
 				this.arc = d3.svg.arc().outerRadius(this.radius * 0.8).innerRadius(this.radius * 0.4);
 				this.outerArc = d3.svg.arc().innerRadius(this.radius * 0.9).outerRadius(this.radius * 0.9);
 		};
 		GmrDonutChart.prototype.createSVG = function(el) {
 				var self = this,
 				  svg = d3.select(el)
 				    .append("svg")
 				    .append("g");

                    svg.attr("transform", "translate(" + self.width / 2 + "," + self.height / 2 + ")");
 				 return svg;
 		};
 		GmrDonutChart.prototype.toSnakeCase = function(str) {
 				return str.toLowerCase().replace(/ /g, "_");
 		};
 		GmrDonutChart.prototype.createSlice = function() {
            var self = this;
            self.svg.append("g").attr("class", "slices");
            var	slice = self.svg.select(".slices").selectAll("path.slice")
              .data(self.pie(self.data), function(d){
                return d.data.lengend_label;
                });

            slice.enter()
              .insert("path")
              .attr("data-name", function(d){
                    return self.toSnakeCase(d.data.lengend_label);
              })
              .style("fill", function(d) {
                return self.color(d.data.lengend_label);
              })
              .attr("class", "slice");

            slice
              .transition().duration(1000)
              .attrTween("d", function(d) {
                    this._current = this._current || d;
                    var interpolate = d3.interpolate(this._current, d);
                    this._current = interpolate(0);
                    return function(t) {
                      return self.arc(interpolate(t));
                    };
              });

            slice.exit().remove();

            if(!self.legend_percent && !self.elData.append_percent){
                var g = self.svg.selectAll(".arc")
                      .data(self.pie(self.data))
                    .enter().append("g")
                      .attr("class", function(d) { return (self.toSnakeCase(d.data.lengend_label) +"_arc arc"); });

                  g.append("text")
                      .attr("transform", function(d) { return "translate(" + self.arc.centroid(d) + ")"; })
                      .attr("dy", ".35em")
                      .style("text-anchor", "middle")
                      .text(function(d) { return d.data.percent; });
 			}
 		};
 		GmrDonutChart.prototype.createTextLabels = function() {
 				var self = this;
                if(self.show_label){
                    self.svg.append("g").attr("class", "labels");

                    var	key = function(d){
                            return d.data.lengend_label;
                        },
                        midAngle = function(d){
                          return d.startAngle + (d.endAngle - d.startAngle)/2;
                        },
                        text = self.svg.select(".labels").selectAll("text").data(self.pie(self.data), key);

                    text.enter()
                      .append("text")
                      .attr("dy", ".35em")
                      .text(function(d){
                            return d.data.label;
                        });


                    text.transition().duration(1000)
                      .attrTween("transform", function(d) {
                        this._current = this._current || d;
                        var interpolate = d3.interpolate(this._current, d);
                        this._current = interpolate(0);
                        return function(t) {
                          var d2 = interpolate(t);
                          var pos = self.outerArc.centroid(d2);
                          pos[0] = self.radius * (midAngle(d2) < Math.PI ? 1 : -1);
                          return "translate("+ pos +")";
                        };
                      })
                      .styleTween("text-anchor", function(d){
                        this._current = this._current || d;
                        var interpolate = d3.interpolate(this._current, d);
                        this._current = interpolate(0);
                        return function(t) {
                          var d2 = interpolate(t);
                          return midAngle(d2) < Math.PI ? "start":"end";
                        };
                      });

                    text.exit().remove();
            }
 		};

 		GmrDonutChart.prototype.createLegends = function() {
            var self = this;
            if(self.show_legend) {
                var str = "<div class='legendWrap'>";
                for(var i=0; i<self.data.length; i++){
                    str += "<span class='legendItem'><span style='background-color:"+self.colors[i]+";' class='legendSqure'></span>";
                    str += self.data[i].lengend_label;
                    if(self.legend_percent) {
                        str += " (" + self.data[i].percent + ")";
                    }
                    str += "</span>";
                }
                str += "</div>";
                this.$element.append(str);
            }
 		};
 		GmrDonutChart.prototype.createTextPolyLines = function() {
            var self = this;
            if(self.show_polyline) {
                self.svg.append("g").attr("class", "lines");

                var midAngle = function (d) {
                        return d.startAngle + (d.endAngle - d.startAngle) / 2;
                    },
                    polyLine = self.svg.select(".lines").selectAll("polyline")
                        .data(self.pie(self.data), function (d) {
                            return d.data.lengend_label;
                        });

                polyLine.enter().append("polyline");

                polyLine.transition().duration(1000)
                    .attrTween("points", function (d) {
                        this._current = this._current || d;
                        var interpolate = d3.interpolate(this._current, d);
                        this._current = interpolate(0);
                        return function (t) {
                            var d2 = interpolate(t);
                            var pos = self.outerArc.centroid(d2);
                            pos[0] = self.radius * 0.95 * (midAngle(d2) < Math.PI ? 1 : -1);
                            return [self.arc.centroid(d2), self.outerArc.centroid(d2), pos];
                        };
                    });

                polyLine.exit().remove();
            }
 		};
 		GmrDonutChart.prototype.createChart = function() {
 				this.createSlice();
 				this.createTextLabels();
 				this.createTextPolyLines();
 				this.createLegends();
 		};

    $.fn.gmrDonutChart = function(config) {
    		this.each(function(idx, item){
    				$(item).data('gmr_donut_chart', new GmrDonutChart(item, config));
    		});
        return this;
    };

}( jQuery, window.d3 ));