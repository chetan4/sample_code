## Sample code using HTML, Javascript, Angular JS, Node JS, Ruby

#### HTML & CSS/SCSS ([Click here](https://github.com/idyllicsoftware/sample_code/tree/master/html_sample))
```
HTML saved page samples are added to "html_sample" directory, from existing project. 
For reference uncompressed js code and screenshot added.

	Used technologies: 
	1) Jquery 
	2) Javascripts 
	3) HTML 
	4) SCSS 
	5) Bootstrap
	
```


#### Angular JS Plugins ([Click here](https://github.com/idyllicsoftware/sample_code/tree/master/angular-js-plugins))
```
	Angular js plugins added for reference to "angular-js-plugins" directory, 
	Used technologies: 

		1) angular 2) angular-ui-bootstrap 

	plugins => 
		1) ui-days-selector => select days on from monday to sunday or all.
		2) ui-numberfield => number field like textfield  with optional configs (min, max)

	for reference uncompressed js code, html demo and screenshot added.

```

#### Node Js + Angular JS ([Click here](https://github.com/idyllicsoftware/sample_code/tree/master/node%20js%20%2B%20angular%20js/vts-demo))
```
	Vehicle Tracking Sample code on map

	used technologies: 
		1) angular 2) angular-ui-bootstrap 3) Jade 4) Node JS
	
	for reference uncompressed js code, html demo and screenshot added.

```

#### Javascript ([Click here](https://github.com/idyllicsoftware/sample_code/tree/master/javascript/trello%20demo%20using%20javascript%20prototypes%20%26%20localstorage))
```
	Trello Sample app using Javascript (Prototype Structure), LocalStorage
		
	used technologies: 
		1) Javascript 2) Jquery 3) Html 4) CSS
	
	for reference uncompressed js code, html demo added.

```

#### Node Js ([Click here](https://github.com/idyllicsoftware/sample_code/tree/master/nodejs))
```

	1. Peapod:
	Node.js app that serves the static pages, and install dependencies
	
	2. Zuleikha - Event Store:
	Backed by RethinkDb as a datastore that publishes events to a Rabbitmq message bus. It's a fairly ghetto implementation and untested.

```

#### Ruby ([Click here](https://github.com/idyllicsoftware/sample_code/tree/master/ruby_remote_control_problem))
```

	Interesting remote control problem solved using Ruby Script.

```

