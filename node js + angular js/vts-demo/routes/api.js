/*
 * Serve JSON to our AngularJS client
 */

// var MongoClient = require('mongodb').MongoClient,
//     db;

// MongoClient.connect('mongodb://****/vts', function(err, dbc) {
//     if(err) throw err;
//     db = dbc;
// });

module.exports = function(app) { 
	
	// login
	app.post('/api/login', function(req, res) {
		var params = req.body;
		if(params.email == "a@a.com" && params.password == "123" ){
			return res.send({status: "success", response_data: {
				username: "a",
				email: "a@a.com"
			}});
		}
		return res.send({status: "error", error_desc: "Invalid email id"});
	});

	// logout
 	app.get('/api/logout', function(req, res) {
 		return res.send({status: "success"});
 	});

	// Get Drive Details
 	app.get('/api/get-drivers', function(req, res) {
		var states = req.query.states;
		if( states ){
			// db.collection('device_logs').find({"state": states, isBlocked: "false"}, {limit:100, fields:{latitude: 1, longitude: 1, state: 1}}).toArray(function(err, docs) {
			//   if(!err){
			//   	return res.send(docs);
			//   }else
			//     return res.send([]);
			// });
 			return res.send([{
 			    	"latitude":13.074127563815,
 			    	"longitude":77.779769897461,
 			    	state: 1
 			    }, {
 			    	"latitude":13.079143861039,
 			    	"longitude":77.796249389648,
 			    	state: 2
 			    }]);
		}else {
			return res.send([]);
		}
    });
	
}
