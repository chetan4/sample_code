# Problem Description:
  Refer 'RemoteControl-Ruby ProblemStatement.docx' for problem statement.

# Pre-Requisite: 
  1. remote_control.rb

# Inputs:
  This program takes three inputs from user:
    1. First input will contain two space delimited numbers, these will form the lowest channel and 
       the highest channel. 
    2. Number of blocked channels followed by sequence of blocked channels (space delimited)
    3. Number of watch list channels followed by sequence of channels that user must view.(space delimited)
 
# Output:
  Prints Minimum number of clicks to get through all the shows

# Run:
  => ruby <filename.rb>
    
# Description: 
    Returns minimum number of clicks required to switch a given set of channels in the given order.
    Skips blocked channels. 
  


This directory contains total three program files:

1. driver.rb : Can be used to run the program on console. 

Run Command: => ruby driver.rb

2. remote_controller.rb : Has the RemoteControl class definition and related methods.

3. rc_spec.rb : Test file for RemoteControl class. 

rspec gem is needed to run this file.

=> gem install rspec
To run the automated test:
=> rspec rc_spec.rb

