ClusterPdfApp.Models = ClusterPdfApp.Models || {};

(function () {
    'use strict';
    ClusterPdfApp.Models.VendorAddress = Backbone.Model.extend({
        initialize: function (attributes) {
        },
        createMarker: function(markerOptions){
            var oThis = this,
                latitude = oThis.get('latitude'),
                longitude = oThis.get('longitude');

            if(latitude && longitude) {
                oThis.map = markerOptions.map;
                var markerConfig =  {
                    map: markerOptions.map,
                    position: new google.maps.LatLng(latitude, longitude),
                    icon: new google.maps.MarkerImage(
                        markerOptions.marker_icon_url,
                        new google.maps.Size( 36, 60 ),
                        new google.maps.Point( 0, 0 ),
                        new google.maps.Point( 18, 0 )
                    )
                };

                var marker = new google.maps.Marker(markerConfig);
                this.set('marker', marker);
                this.set('is_marker_visible', true);

                var contentString = '<div id="marker-content-wrap">'+
                        "<b>Vendor: </b>" + oThis.get('vendor_name') + "<br/>"+
                        "<b>Address: </b>" + oThis.get('vendor_address')+
                    '</div>';

                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });

                marker.addListener('click', function() {
                    infowindow.open( markerOptions.map, marker );
                });
            }
        },
        showMarker: function(){
            var marker = this.get('marker');
            marker.setMap( this.map );
            this.set('is_marker_visible', true);
        },
        hideMarker: function(){
            var marker = this.get('marker');
            marker.setMap( null );
            this.set('is_marker_visible', false);
        }

    });
})();
