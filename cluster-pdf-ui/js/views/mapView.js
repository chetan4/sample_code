ClusterPdfApp.Views = ClusterPdfApp.Views || {};

(function () {
    'use strict';

    ClusterPdfApp.Views.MapView = Backbone.View.extend({
        mapOptions: {
            zoom: 12,
            center: new google.maps.LatLng(33.8435370000, -118.2568580000),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        },
        events: {
            "click #download_map": "downloadMap",
        },
        initialize: function () {},
        render: function () {
            var oThis = this,
                template = $("#mapViewTpl").html(),
                templateData = {};

            oThis.$el.html(Mustache.to_html(template, templateData));
            oThis.initMap();
            oThis.initListeners();
        },
        initMap: function(){
            var oThis = this,
                map_wrap = oThis.$el.find("#vendorMapId")[0],
                trafficLayer = new google.maps.TrafficLayer();

            oThis.map = new google.maps.Map(map_wrap, oThis.mapOptions);
            trafficLayer.setMap(oThis.map);
            oThis.createMarkers();
        },
        initListeners: function(){
            var oThis = this;
            google.maps.event.addListenerOnce(oThis.map, 'bounds_changed', function(){
                oThis.updateViewportCount();
            });

            google.maps.event.addListener(oThis.map, 'zoom_changed', function(){
                oThis.updateViewportCount();
            });

            google.maps.event.addListener(oThis.map, 'dragend', function(){
                oThis.updateViewportCount();
            });

            oThis.listenTo(oThis.collection, 'markers_updated', function(){
                oThis.updateViewportCount();
            })
        },
        updateViewportCount: function(){
            this.$el.find("#totalMarkerId span").html(this.getViewportCount());
        },
        getViewportCount: function(){
            return this.collection.getViewportCount();
        },
        createMarkers: function(){
            var oThis = this;
            oThis.collection.createMarkers(oThis.map);
        },
        downloadMap: function(){
            var oThis = this,
                node = oThis.$el.find("#vendorMapId")[0];
            $.pageLoader();
            domtoimage.toPng(node)
                .then (function (dataUrl) {
                    var img = new Image();
                    img.src = dataUrl;
                    img.width = 800;
                    img.height = 450;
                    img.setAttribute('crossOrigin', 'anonymous');
                    oThis.generatePDF(img);
                })
                .catch(function (error) {
                    $.pageLoader('hide');
                    alert(error);
                });
        },
        generatePDF: function(img){
            var doc = new jsPDF('landscape');
            doc.setFontSize(20);
            doc.text(25, 25, "Cluster PDF");
            doc.addImage(img, 'PNG', 25, 35);
            doc.save('cluster_pdf.pdf');
            $.pageLoader('hide');
        }
    });

})();

