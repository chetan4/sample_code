ClusterPdfApp.Views = ClusterPdfApp.Views || {};

(function () {
    'use strict';

    ClusterPdfApp.Views.VendorView = Backbone.View.extend({
        events: {
            "change .vendorCheckox": "onVendorCheckChange",
        },
        initialize: function () {},
        render: function () {
            var oThis = this,
                template = $("#vendorViewTpl").html(),
                vendors = this.collection.getVendors(),
                templateData = {
                    vendors: vendors
                };

            oThis.$el.html(Mustache.to_html(template, templateData));
        },
        onVendorCheckChange: function(event){
            var vendor_id = event.target.value,
                state = event.target.checked;

            this.collection.toggleMarkersByVendorId(vendor_id, state);
        }
    });

})();

