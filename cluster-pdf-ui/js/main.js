window.ClusterPdfApp = {
    Models: {},
    Collections: {},
    Views: {},
    ENUMS: {},
    Routers: {},
    init: function () {
        var oThis = this,
            vendorCollection = new ClusterPdfApp.Collections.VendorAddressCollection(ClusterPdfApp.vendorAddressList);

        oThis.vendorView = new ClusterPdfApp.Views.VendorView({
            el: $("#vendor_address_wrapper"),
            collection: vendorCollection
        });
        oThis.vendorView.render();

        oThis.mapView = new ClusterPdfApp.Views.MapView({
            el: $("#map_wrapper"),
            collection: vendorCollection
        });
        oThis.mapView.render();
        $.pageLoader('hide');
    }
};

$(document).ready(function () {
    'use strict';
    $.pageLoader();
    ClusterPdfApp.init();
});
