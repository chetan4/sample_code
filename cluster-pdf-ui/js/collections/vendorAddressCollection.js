ClusterPdfApp.Collections = ClusterPdfApp.Collections || {};

(function () {
    'use strict';

    ClusterPdfApp.Collections.VendorAddressCollection = Backbone.Collection.extend({
        model: ClusterPdfApp.Models.VendorAddress,
        vendor_marker_hash: {},
        get_marker_url: function(number){
            return "images/markers/marker"+number+".png";
        },
        getVendors: function(){
            var oThis = this,
                vendors = [],
                vendor_model, marker_icon_url,
                count = 1,
                list = oThis.groupBy('vendor_id');

            for(var id in list){
                marker_icon_url = oThis.get_marker_url(count);
                oThis.vendor_marker_hash[id] = marker_icon_url;
                vendor_model = list[id][0];
                vendors.push({
                    id: id,
                    marker_icon_url: marker_icon_url,
                    name: vendor_model.get('vendor_name')
                });
                count++;
            }
            return vendors;
        },
        createMarkers: function(map){
            var oThis = this,
                sub_model,
                marker_icon_url, vendor_id,
                markerOptions, i;

            oThis.map = map;

            for(i=0; i<oThis.models.length; i++){
                sub_model = oThis.models[i];
                vendor_id = sub_model.get('vendor_id');
                marker_icon_url = oThis.vendor_marker_hash[vendor_id];
                markerOptions = {
                    map: map,
                    marker_icon_url: marker_icon_url
                };
                sub_model.createMarker(markerOptions);
            }
        },
        toggleMarkersByVendorId: function(vendor_id, state){
            var oThis = this,
                sub_models = oThis.filter({'vendor_id': Number(vendor_id)}),
                fnName = state ? 'showMarker' : 'hideMarker';

            sub_models.forEach(function(item, index){
                item[fnName]();
            });
            oThis.trigger('markers_updated');
        },
        getViewportCount: function(){
            var oThis = this,
                count = 0,
                bounds = oThis.map.getBounds(),
                marker;
            this.models.forEach(function(item, index){
                marker = item.get('marker');
                if(bounds && item.get('is_marker_visible') && bounds.contains(marker.getPosition())){
                    count++;
                }
            });
            return count;
        }
    });

})();
